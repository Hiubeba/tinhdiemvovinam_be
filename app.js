require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const http = require('http');
const { Server } = require('socket.io');

const app = express();
const port = process.env.PORT || 3000;

app.use(bodyParser.json());
app.use(cors({
    origin: '*'
}));

let scores = {
    red: 0,
    blue: 0
};

let judgeVotes = {
    red: Array(4).fill(undefined),
    blue: Array(4).fill(undefined)
};

const server = http.createServer(app);
const io = new Server(server, {
    cors: {
        origin: '*'
    }
});

io.on('connection', (socket) => {
    console.log('a user connected');
    socket.on('disconnect', () => {
        console.log('user disconnected');
    });
});

const broadcast = (data) => {
    io.emit('scoreUpdate', data);
};

// Middleware to check if judge ID is valid
app.use((req, res, next) => {
    if (req.body.judgeId < 1 || req.body.judgeId > 4) {
        return res.status(400).send('Invalid judge ID');
    }
    next();
});

app.post('/vote', (req, res) => {
    const { judgeId, team, vote } = req.body;
    if (!['red', 'blue'].includes(team) || ![1, 2, -1, -2].includes(vote)) {
        return res.status(400).send('Invalid team or vote');
    }

    // Record the vote
    judgeVotes[team][judgeId - 1] = vote;

    // Judge 1's vote is always counted
    if (judgeId === 1) {
        scores[team] += vote;
        judgeVotes[team] = Array(4).fill(undefined);
        broadcast({ team, score: scores[team] });
        return res.status(200).send(`Score updated for team ${team}. New score: ${scores[team]}`);
    }

    // Count votes from Judge 2, 3, and 4
    const judge2Vote = judgeVotes[team][1];
    const judge3Vote = judgeVotes[team][2];
    const judge4Vote = judgeVotes[team][3];

    const validVotes = [judge2Vote, judge3Vote, judge4Vote].filter(vote => vote !== undefined);
    const voteCounts = validVotes.reduce((acc, vote) => {
        acc[vote] = (acc[vote] || 0) + 1;
        return acc;
    }, {});

    const agreedVote = Object.entries(voteCounts).find(([vote, count]) => count >= 2);

    if (agreedVote) {
        const [vote, count] = agreedVote;
        scores[team] += parseInt(vote);
        judgeVotes[team] = Array(4).fill(undefined);
        broadcast({ team, score: scores[team] });
        return res.status(200).send(`Score updated for team ${team}. New score: ${scores[team]}`);
    }

    res.status(200).send(`Vote recorded for team ${team}`);
});

app.post('/reset', (req, res) => {
    scores = {
        red: 0,
        blue: 0
    };
    judgeVotes = {
        red: Array(4).fill(undefined),
        blue: Array(4).fill(undefined)
    };
    broadcast({ red: scores.red, blue: scores.blue });
    res.status(200).send('Scores reset');
});

app.get('/scores', (req, res) => {
    res.status(200).send(scores);
});

app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});
